import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { ListComponent } from './list/list.component';
import { ListItemComponent } from './list-item/list-item.component';
import { HelpComponent } from './help/help.component';
import { Routes, RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    ListComponent,
    ListItemComponent,
    HelpComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path:'', component: ListComponent },
      { path:'list', component: ListComponent },
      { path:'list-item', component: ListItemComponent },
      { path:'help', component: HelpComponent },
      { path:'**', component: ListComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
